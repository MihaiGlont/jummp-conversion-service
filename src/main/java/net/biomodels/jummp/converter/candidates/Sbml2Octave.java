package net.biomodels.jummp.converter.candidates;

import net.biomodels.jummp.converter.ConversionInvoker;
import net.biomodels.jummp.converter.request.ConversionRequest;
import org.sbfc.converter.exceptions.WriteModelException;
import org.sbfc.converter.models.OctaveModel;
import org.sbfc.converter.models.SBMLModel;
import org.sbfc.converter.sbml2octave.SBML2Octave;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sbml2Octave extends GeneralCandidate implements ConversionInvoker {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public boolean convert(ConversionRequest request) {
        String inputModelPath = getInputFileForRequest(request);
        String outputFile = request.getResult().getOutputFile().toString();
        logger.debug("Converting {} to {}", inputModelPath, outputFile);
        SBMLModel sbmlModel = resolveSbmlModel(inputModelPath);
        if (null != sbmlModel) {
            OctaveModel octaveModel = new SBML2Octave().octaveExport(sbmlModel);
            try {
                octaveModel.modelToFile(outputFile);
                return true;
            } catch (WriteModelException e) {
                logger.error("Failed to create output file {} for request {}", outputFile, request);
            }
        }
        return false;
    }
}
