package net.biomodels.jummp.converter.request;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;
import net.biomodels.jummp.converter.ModelFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConversionRequest implements DataSerializable {
    private ModelFormat from;
    private ModelFormat to;
    private long revision;
    private Path inputFile;
    private ConversionResult result;

    public ConversionRequest(ModelFormat from, ModelFormat to, long revision, Path inputFile) {
        this.from = from;
        this.to = to;
        this.revision = revision;
        this.inputFile = inputFile;
        result = new ConversionResult();
    }

    public ModelFormat getFrom() {
        return from;
    }

    public ModelFormat getTo() {
        return to;
    }

    public ConversionResult getResult() {
        return result;
    }

    public long getRevision() {
        return revision;
    }

    public void setConvertedFileLocation(String location) {
        result.setOutputFile(Paths.get(location));
    }

    public Path getInputFile() {
        return inputFile;
    }

    public String toString() {
        return String.format("Revision %d (%s) from %s to %s", revision,
                inputFile.getFileName().toString(), from.toString(), to.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConversionRequest request = (ConversionRequest) o;

        return revision == request.revision
                && to.equals(request.to)
                && inputFile.getFileName().equals(request.inputFile.getFileName());
    }

    @Override
    public int hashCode() {
        int result = to.hashCode();
        result = 31 * result + (int) (revision ^ (revision >>> 32));
        result = 31 * result + inputFile.getFileName().hashCode();
        return result;
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeLong(revision);
        out.writeUTF(inputFile.toUri().toString());
        from.writeData(out);
        to.writeData(out);
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        revision = in.readLong();
        String inputFileUri = in.readUTF();
        try {
            inputFile = Paths.get(new URI(inputFileUri));
        } catch (URISyntaxException e) {
            throw new IOException(e);
        }
        to = new ModelFormat();
        to.readData(in);
        from = new ModelFormat();
        from.readData(in);
    }
}
