package net.biomodels.jummp.restapi;

import net.biomodels.jummp.converter.ConversionService;
import net.biomodels.jummp.converter.ModelFormat;
import net.biomodels.jummp.converter.request.ConversionRequest;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/converter")
public class ConversionController {
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

    private final ConversionService conversionService;

    public ConversionController(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    @Cacheable(value = "conversionRequestsCache", sync = true)
    @SuppressWarnings("unused")
    @RequestMapping(value = "/convert/{revision}", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    public Map<String, String> convert(
            @PathVariable Integer revision,
            @RequestParam(name = "from", defaultValue = "sbml") String inputFormat,
            @RequestParam(name = "to", defaultValue = "octave") String outputFormat,
            @RequestParam(name = "file") String filePath) {

        ModelFormat in = new ModelFormat(inputFormat);
        ModelFormat out = new ModelFormat(outputFormat);

        logger.info("Converting to {} revision {}, file {}", outputFormat, revision, filePath);
        Path modelFile;
        try {
            modelFile = Paths.get(new URI(filePath));
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Location '" + filePath + "' is not a URI.");
        }
        if (!Files.isReadable(modelFile) || Files.isDirectory(modelFile)) {
            throw new IllegalArgumentException("Location '" + filePath + "' is not a readable file.");
        }
        ConversionRequest request = new ConversionRequest(in, out, revision, modelFile);
        boolean outcome = conversionService.handle(request);
        String response = outcome ? request.getResult().getOutputFileUri() : null;
        logger.info("Request {} produced {}", request, response);
        return Collections.singletonMap("result", response);
    }
}
