package net.biomodels.jummp.restapi;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/indexing")
public class IndexingHandler {

    @RequestMapping(value = "hello", method = RequestMethod.GET)
    public String hello() {
        return "Hello World!";
    }
}
